import { Component, OnInit } from '@angular/core';
import { VideoUrlService } from '../video-url.service';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser'

@Component({
  selector: 'app-video-view',
  templateUrl: './video-view.component.html',
  styleUrls: ['./video-view.component.scss']
})
export class VideoViewComponent implements OnInit {
  safeUrl: SafeResourceUrl;

  constructor(private _videoUrlService: VideoUrlService, private _sanitizer: DomSanitizer){}

  ngOnInit() {
    this._videoUrlService.getEventEmitter().subscribe(
      videoInfo => {
        this.generateEmbedUrl(videoInfo.id);
      }
    )
  }
  generateEmbedUrl(videoUrl){
    var unsafeEmbedUrl = this.generateUnsafeEmbedUrl(videoUrl);
    this.safeUrl = this.sanitizeRessource(unsafeEmbedUrl);
  }

  generateUnsafeEmbedUrl(id){
    return "https://www.youtube.com/embed/" + id;
  }
  sanitizeRessource(unsafeRessource){
    return this._sanitizer.bypassSecurityTrustResourceUrl(unsafeRessource);
  }
}
