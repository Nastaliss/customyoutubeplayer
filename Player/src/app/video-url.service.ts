import { Injectable, EventEmitter} from '@angular/core';
import { VideoInfo } from "./video-info";

@Injectable({
  providedIn: 'root'
})
export class VideoUrlService {
  videoUrlUpdated: EventEmitter<VideoInfo> = new EventEmitter();

  setVideoUrl(info: VideoInfo){
    this.videoUrlUpdated.emit(info)
  }

  getEventEmitter(){
    return this.videoUrlUpdated;
  }
}
