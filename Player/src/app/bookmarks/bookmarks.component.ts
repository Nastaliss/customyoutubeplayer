import { Component, OnInit } from '@angular/core';
import { VideoUrlService } from '../video-url.service';
import { VideoInfo } from "../video-info";

@Component({
  selector: 'app-bookmarks',
  templateUrl: './bookmarks.component.html',
  styleUrls: ['./bookmarks.component.scss']
})
export class BookmarksComponent implements OnInit {
  bookmarks: VideoInfo[];
  videoInfo: VideoInfo;

  constructor(private _videoUrlService: VideoUrlService) {
    this.bookmarks = [];
    this.videoInfo = null;
  }

  ngOnInit() {
    this._videoUrlService.getEventEmitter().subscribe(
      videoInfo => {
        this.videoInfo = videoInfo;
      }
    )
  }
  onSave(){
    if(this.videoInfo !== null){
      if (!this.bookmarks.includes(this.videoInfo)){
        this.bookmarks.push(this.videoInfo);
      }
    }
    return false;
  }
  onLoad(videoInfo){
    this._videoUrlService.setVideoUrl(videoInfo);
  }

  onDelete(video){
    var indexToDelete = this.bookmarks.indexOf(video);
    if(indexToDelete !==-1){
      this.bookmarks.splice(indexToDelete, 1);
    }
  }
}
