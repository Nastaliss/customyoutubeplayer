import { Component } from '@angular/core';
import { VideoUrlService } from './video-url.service';
import { SearchBarComponent } from './search-bar/search-bar.component'
import { VideoViewComponent } from './video-view/video-view.component'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers:[VideoUrlService],
})
export class AppComponent {
  title = 'Player';
}
