export class VideoInfo {
  url: string;
  error: string;
  id: string;

  constructor(url: string){
    // console.log(url)
    // console.log(id)
    this.error = null;
    this.url = url;
    if(this.urlIsValid()){
      this.id = this.generateId();
    }

  }

  urlIsValid(){
    if(!this.videoDelimiterInUrl()){
      return this.setError("Not a youtube video");
    }
    if(!this.urlIsYoutube()){
      return this.setError("Not a youtube URL");
    }
    if(!this.idIsValid()){
      return this.setError("Invalid videoID");
    }
    return true;
  }

  videoDelimiterInUrl(){
    return this.url.includes(this.getVideoDelimiter());
  }

  urlIsYoutube(){
    var prefix = this.url.split(this.getVideoDelimiter())[0];
    return prefix === "https://www.youtube.com/watch";
  }

  idIsValid(){
    return /^[a-zA-Z0-9_-]{11}$/.test(this.generateId());
  }

  getVideoDelimiter(){
    return "?v="
  }

  generateId(){
    return this.url.split(this.getVideoDelimiter())[1]
  }

  setError(errorDescription: string){
    this.error = errorDescription;
    return false;
  }
}
