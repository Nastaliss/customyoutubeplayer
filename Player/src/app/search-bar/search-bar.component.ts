import { Component, Input } from '@angular/core';
import { VideoUrlService } from '../video-url.service';
import { VideoInfo } from "../video-info";

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss']
})
export class SearchBarComponent {

  videoInfo: VideoInfo;
  querryError: string;

  constructor(private _videoUrlService: VideoUrlService){}

  onVideoUrlSubmit(url){
    this.videoInfo = new VideoInfo(url);
    if(this.videoInfo.error === null){
      this._videoUrlService.setVideoUrl(this.videoInfo)
    }
    return false;
  }

}
