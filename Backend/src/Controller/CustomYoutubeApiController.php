<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\History;
use Symfony\Component\HttpFoundation\Request;
use Psr\Log\LoggerInterface;
class CustomYoutubeApiController extends AbstractController
{

  private $logger;
    /**
     * @Route("/", name="default")
     */
    public function index()
    {
        return $this->render('custom_youtube_api/index.html.twig', [
            'controller_name' => 'CustomYoutubeApiController',
        ]);
    }

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }
    /**
     * @Route("/fetch-history", name="fetch history")
     */
    public function fetchHistory(Request $request){
      $repository = $this->getDoctrine()->getRepository(History::class);

      $historyDb = $repository->findAll();
      $historyItemsArray = array();
      foreach($historyDb as $historyDbItem){
         $historyItem = array(
          'id' => $historyDbItem->getVideoId(),
          'url' => $historyDbItem->getVideoUrl(),
        );
        array_push($historyItemsArray, $historyItem);
      }
      $response = new JsonResponse($historyItemsArray);

      $response->headers->set('Content-Type', 'application/json');

      return $response;
    }

    /**
     * @Route("/store-history", name="store history")
     */
    public function storeHistory(Request $request){
      $entityManager = $this->getDoctrine()->getManager();

      $url = $request->query->get('url');
      $id = $request->query->get('id');

      $historyEntry = new History();
      $historyEntry->setVideoUrl($url);
      $historyEntry->setVideoId($id);

      $entityManager->persist($historyEntry);
      $entityManager->flush();

      return new JsonResponse(
        array(
          'action' => "store-history",
          'status' => "success",
          'id' => $id,
          'url' => $url,
        )
      );
    }

}
