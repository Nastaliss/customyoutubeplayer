import { Component, OnInit } from '@angular/core';
import { VideoUrlService } from '../video-url.service';
import { VideoInfo } from "../video-info";
import { HttpClient, HttpParams} from '@angular/common/http';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})
export class HistoryComponent implements OnInit {
  history: VideoInfo[];
  videoInfo: VideoInfo;

  constructor(private _videoUrlService: VideoUrlService, private http: HttpClient) {
      this.history = []
      this.videoInfo = null;
  }

  ngOnInit() {
    this.http.get('http://127.0.0.1:8000/fetch-history')
    .subscribe((res: Array<any>) => {
      res.forEach(historyEntry => {
        console.log(historyEntry.id)
        var loadedHistoryVideoInfo = new VideoInfo(historyEntry.url);
        this.history.push(loadedHistoryVideoInfo)
      })
    });

    this._videoUrlService.getEventEmitter().subscribe(
      videoInfo => {
        let parameters = new HttpParams()
        .set("id", videoInfo.id).set("url", videoInfo.url);
        console.log(parameters)
        this.http.get('http://127.0.0.1:8000/store-history', {params: parameters})
        .subscribe();
        this.history.push(videoInfo);
      }

    )
  }

  onLoad(videoInfo){
    console.log(videoInfo)
    this._videoUrlService.setVideoUrl(videoInfo);
  }

  onDelete(){
    this.history = [];
  }
}
